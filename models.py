from peewee import *
from flask_login import UserMixin

database = SqliteDatabase("database.sqlite3")

class BaseModel(Model):
    class Meta:
        database = database

class UsersList(BaseModel, UserMixin): 
    pseudo = CharField(unique=True)
    password = CharField()

class FlowList(BaseModel):
    idUser = BigIntegerField()
    lienFlux = CharField()

def create_tables():
    with database:
        database.create_tables([UsersList,FlowList ])
        print('Initialized the database')

def drop_tables():
    with database:
        database.drop_tables([UsersList, FlowList ])
        print('Dropped tables from database')
