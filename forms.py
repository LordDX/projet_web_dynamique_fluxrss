from flask_wtf import FlaskForm
from wtforms import StringField, SelectField
from wtforms.fields.html5 import DateField
from wtforms.validators import DataRequired, Length
from wtforms.widgets import PasswordInput
from feedparser import *

class Login(FlaskForm):
    pseudo = StringField('Pseudo ', validators=[DataRequired(), Length(min=3, max=20)])
    password = StringField('Mot de passe ', widget=PasswordInput(hide_value=True))
    pass

class Register(FlaskForm):
    pseudo = StringField('Pseudo ', validators=[DataRequired(), Length(min=3, max=20)])
    password = StringField('Mot de passe', widget=PasswordInput(hide_value=True))
    pass

class AddFlux(FlaskForm):
    lien = StringField('Lien du flux ', validators=[DataRequired()])
    pass