from flask import Flask, render_template, request, redirect, flash, url_for
from flask_login import LoginManager, UserMixin, login_user, login_required, logout_user, current_user
from models import *
from forms import *
import os,hashlib,feedparser


app = Flask(__name__)
app.config['SESSION_TYPE'] = 'memcached'
app.config['SECRET_KEY'] = 'super secret key'

login_manager = LoginManager() 
login_manager.init_app(app)


@login_manager.user_loader
def load_user(user_id):
    return UsersList.get(id=user_id)

"""
  Route vers la page index.
"""
@app.route('/')
def index():
    return render_template('index.html')

"""
  Inscription d'un utilisateur dans la base de donnée
"""
@app.route('/register', methods=['GET', 'POST', ])
def register():
    form = Register()
    if form.validate_on_submit():
        pseudo = form.pseudo.data
        motDePasse = form.password.data
        motDePasse = hashlib.sha1(motDePasse.encode()).hexdigest()
        user = UsersList.select().where(UsersList.pseudo == pseudo).first()
        if(user == None):
            utilisateur = UsersList()
            utilisateur.pseudo = pseudo.upper()
            utilisateur.password = motDePasse
            utilisateur.save()
            return redirect(url_for('index'))
        else:
            flash("Cette utilisateur existe déjà au sein de la base")
            return redirect(url_for('Register'))
    return render_template('register.html', form=form)    

"""
  Page de connexion, si l'utilisateur existe alors il ce connectera
"""
@app.route('/login', methods=['GET', 'POST', ])
def login():
    form = Login() 
    if form.validate_on_submit():
        pseudoConnect = form.pseudo.data.upper()
        motDePasseConnect = hashlib.sha1(form.password.data.encode()).hexdigest()
        print(motDePasseConnect)
        user = UsersList.select().where((UsersList.pseudo == pseudoConnect) & (UsersList.password == motDePasseConnect)).first()
        print(user)
        if (user == None):
            print("Votre mot de passe ou votre identifiants est incorrecte")
        else:
            login_user(user)
            current_user.idUtilisateur = user.id
            return redirect(url_for('accueil'))
    return render_template('login.html', form=form)

"""
  Page d'acceuil du compte utilisateur, ici on pourras voirs les flux de l'utilisateur connecter
"""
@app.route('/accueil')
@login_required
def accueil():
    listeFluxParse = []
    listeFluxUser = FlowList.select().where(FlowList.idUser == current_user.id)
    for item in listeFluxUser :
        fluxparse = feedparser.parse(item.lienFlux) 
        listeFluxParse.append(fluxparse)
    return render_template('accueil.html',listeFluxParse=listeFluxParse,listeFluxUser=listeFluxUser,zip=zip(listeFluxParse,listeFluxUser))

"""
  Ajoutes des flux, il faudra ajouter l'url comme demander
"""
@app.route('/addFlux', methods=['GET', 'POST', ])
@login_required
def addFlux():
    form = AddFlux()
    if form.validate_on_submit():
        monfluxBDD = FlowList.select().where((FlowList.lienFlux == form.lien.data) & (FlowList.idUser == current_user.id)).first()
        if(monfluxBDD == None):
            flow = FlowList()
            flow.idUser = current_user.id
            flow.lienFlux = form.lien.data
            flow.save()
            flash('FLux ajouté')
            return redirect(url_for('accueil'))   
        else:
            return redirect(url_for('AddFlux'))
    return render_template('addFlux.html',form=form)

"""
  Suppressions des flux, on supprimera le flux pour l'utilisateur courant
"""
@app.route('/supprFlux', methods=['GET','POST'])
@login_required
def supprFlux():
    idFluxUser = request.args.get('idFluxUser')
    linkFlux = request.args.get('linkFlux')
    requete = FlowList.delete().where((FlowList.idUser == idFluxUser) & (FlowList.lienFlux == linkFlux))
    requete.execute()
    return redirect(url_for('accueil'))

"""
  Page pour visionner c'est flux
"""
@app.route('/showflux', methods=['GET', 'POST'])
@login_required
def showflux():
    linkFlux = request.args.get('linkFlux')
    fluxparse = feedparser.parse(linkFlux)
    element = feedparser.parse(linkFlux).entries
    return render_template('showflux.html',fluxparse=fluxparse, element=element)

"""
  deconnexion
"""
@app.route('/logout')
@login_required
def logout():
    logout_user()
    return redirect(url_for('index'))

"""
  COMMANDE CLI
"""
@app.cli.command()
def initdb():
    create_tables()

@app.cli.command()
def dropdb():
    drop_tables()

@app.cli.command()
def addlogin():
    UsersList.create(pseudo="test",password="test")
    print("Users created")