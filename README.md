--------------------------------------------------------------------------------------------

Ce projet à été réalisé pour le cours de python par Deston Charle, Issam El Yaakoubi et Sully Dumont

--------------------------------------------------------------------------------------------
Pour lancer le projet il faudra utiliser la commande "flask run".
Les dépendances sont retrouvable dans le pipfile sinon démarrer pipenv avec la commande "pipenv shell".
Sans oublier d'installer au préalable pipenv avec la commande "pip install pipenv".
--------------------------------------------------------------------------------------------

App.py contient les routes du programmes
Forms.py contient les définitions des différentes WTForms
models.py générera grâce à peewee une base de données

--------------------------------------------------------------------------------------------

Un utilisateur test à été crée avec des flux déjà ajouter au besoin, les identifiants sont Login : "test" MDP : "test"

--------------------------------------------------------------------------------------------

Le projet à été réaliser avec les outils mise à dispositions au cours de l'année, de l'aide à été apporté pour la finalisation de ce projet.



